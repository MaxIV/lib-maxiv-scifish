# lib-maxiv-scifish

<img align="center" src="https://nox.apps.okd.maxiv.lu.se/widget?conda_package=scifish&rpm_package=python36-scifish"/>

SciFish (Scientific Form Implementation Service Helper) is a library that is
compatible with the latest SciCat format and can be used by a recorder that
wants to add a scan to the database. This library talks to SciDog to retrieve
the scientific metadata and handles all communication with SciCat. Most
variables are retrieved from the environment, it requires a PathFixer device to
get the information about the proposal.


## SciCat dict

The Scicatrecorder sends a dict through Kafka to Scicat with the following
mandatory items:

| Name               | Type   | Info                                                                                    |
| -----              | -----  | ----                                                                                    |
| uuid               | string | created in SciFish, unique ID for the dataset                                           |
| scientificMetadata | dict   | dict containing the scientific metadata variables (configuration generated from SciDog) |
| creationTime       | string | unix timestamp of the time this dataset was created                                     |
| dataFormat         | string | data format                                                                             |
| files              | list   | list of dicts with `path`, `size` and `time` in isoformat                               |
| sampleId           | string | Id of a sample                                                                          |
| datasetName        | string | shown name of dataset, in the sardana recorder: "ScanID xxx - scanFile.h5"              |
| event              | string | <topic>-experiment-began or <topic>-experiment-ended                                    |
| sourceFolder       | string | folder where the files are located                                                      |
| description        | string | short description, in this recorder: "Recorded with the scicatrecorder at <beamline>"   |
| proposalId         | string | proposalId from the Pathfixer device                                                    |


## Deployment

The deployment is done using Ansible. It is required to have the correct Kafka
certificates and private key for each beamline, these can be obtained by asking
IMS to set up a Kafka Consumer for the specific beamline. Currently, each
beamline has a topic `<beamline>` and a topic `<beamline>-test` for testing
purposes. The certificate and key for each beamline can be stored in the
cfg-maxiv-ansible repo under `inventory/group_vars/<beamline>-ec.yaml`
([example](https://gitlab.maxiv.lu.se/kits-maxiv/ansible-galaxy/cfg-maxiv-ansible/-/blob/master/inventory/group_vars/cosaxs-ec.yml)). The
private key should be encrypted by using the ansible vault. The certificates can
be deployed by using the Control System (cs) playbook with the `scicat` tag on
the target ec machine where the scicatrecorder will be running.


## Usage

``` python
from scifish import SciFish

## On start of the scan
scifish = SciFish() 
# Optional arguments: 
# Kafka certificates: "ssl_cafile", "ssl_certfile", "ssl_keyfile" (otherwise it takes the default, /etc/ssl/certs/kafka/*) 
# "beamline" (used by SciFly, since it is not running on a server where it has a connection to the tango database. Using this will retrieve values through Taranta GraphQL)
# "pathfixer" (give tango device name if you want to connect to a specific one, otherwise defaults to the first one it can find in the database)
# "test" (send to the test database instead of production, make sure you have the right certificates)
# "metaTimestampsEnabled" (adds timestamp start_time upon calling start_scan and end_time upon calling end_scan, by default enabled)
scifish.start_scan(datasetName) # Starts a scan, optionally give the datasetName here

## During the scan, anytime before finalizing
# SciCat data
scifish.scicat_data.datasetName = "" # Mandatory, give your dataset a name!
scifish.scicat_data.dataFormat = "" # Optional
scifish.scicat_data.files = [{path: "/tmp/data.h5", size: 0, time: datetime.now().isoformat()}] # Optional, list of where the files can be found. 
scifish.scicat_data.sampleId = "" # Optional, needs to have the sample stored in SciCat with its ID to link to it
scifish.scicat_data.sourceFolder = "" # Optional, by default intialized with the source folder from the PathFixer
scifish.scicat_data.description = "" # Optional, default: "Recorded with the scicatrecorder at <beamline>"

# Show the dict
scifish.show() # Returns the current dataset that has been written so far

# Send start
scifish.send_start() # Can only be done once, after this, no variables but the environment_data/scientificMetadata can be updated (!)

# Environment data
scifish.environment_data.detectors = [] # Optional, list of detectors used
scifish.environment_data.scanID = "" # Optional
scifish.environment_data.title = "" # Optional, title of the scan (sardana: ascan mot01 0 10 10 0.05)

# Update scientific Metadata
scifish.scicat_data.scientificMetadata.update({"Ring current": {"value": 3, "unit": "mA"}}) # One variable with unit
scifish.scicat_data.scientificMetadata.update({"Branch selected": "A"}) # One variable without unit
scifish.scicat_data.scientificMetadata.update(
    {"Machine": {
        {"Ring current": {"value": 3, "unit": "mA"}},
        {"Temperature": {"value": 22, "unit": "degC"}},
        {"Branch selected": {"value": "A", "unit": ""}}
      }
    }
) # Group of variables


## On end of the scan
scifish.end_scan() # Writes scientific metadata from SciDog, sends off to Kafka. Returns buffer size, 0 means all is okay, otherwise it will tell you how many scans are left in the buffer when Kafka is unavailable. 

scifish.fish() # Find out for yourself ;-)

```
