from scifish.user_info import UserInfo
from copy import copy 

class Data:
    __isfrozen = False
    def __init__(self, **kwargs):
        for key, value in template.items():
            setattr(self, key, copy(value))

        self.__isfrozen = True

        for key, value in kwargs.items():
            if key in ["beamline", "pathfixer"]:
                pass
            else:
                setattr(self, key, value)

        beamline = kwargs.get("beamline", None)
        pathfixer = kwargs.get("pathfixer", None)
        self._get_user_info(beamline, pathfixer)

    def __setattr__(self, key, value):
        if self.__isfrozen and not hasattr(self, key) and key not in optional_keys:
            raise NotImplementedError(str(key) + " is not a valid SciCat key")
        object.__setattr__(self, key, value)

    def _get_user_info(self, beamline, pathfixer):
        user_data = UserInfo(beamline, pathfixer)
        for key, value in user_data.get().items():
            setattr(self, key, value)

    def to_dict(self):
        data = vars(self)
        return {x: data[x] for x in data if x not in "_Data__isfrozen"}

template = {
    "uuid": "",
    "scientificMetadata": dict(),
    "creationTime": "",
    "dataFormat": "",
    "files": [],
    "sampleId": "",
    "datasetName": "",
    # From user_info
    "event": "",
    "sourceFolder": "",
    "description": "",
    "proposalId": "",
}

optional_keys = ["comment"]
