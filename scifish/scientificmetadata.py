import math
import requests
import logging

from scifish.config import SCIDOG_URL, WRITE_METADATA_BEGIN

class ScientificMetadata:
    def __init__(self, beamline, value_retriever):
        self._value_retriever = value_retriever

        url = SCIDOG_URL + beamline
        try:
            r = requests.get(url).json()
            self.config = r["data"]
        except:
            logging.warning("Cannot reach SciDog, no Scientific Metadata available.")
            self.config = {}

        self.begin = None

        if beamline in WRITE_METADATA_BEGIN:
            try:
                self.begin = self.get_begin()
            except Exception as exc:
                logging.warning("No Scientific Metadata found: %s" % (exc))


    def get_begin(self):
        begin = dict()

        for item in self.config:
            if item["Group"] not in begin:
                begin[item["Group"]] = dict()
            try:
                begin[item["Group"]][item["Name"]], format = self._value_retriever.retrieve_value(item["Device"], item["Attribute"])
            except:
                begin[item["Group"]][item["Name"]] = ""

            try:
                begin[item["Group"]][item["Name"]] = (format % begin[item["Group"]][item["Name"]]).strip()
            except:
                begin[item["Group"]][item["Name"]] = str(begin[item["Group"]][item["Name"]])

        return begin

    def get(self, environment):
        tangoattrs = dict()

        tangoattrs.update({"title": environment["title"]})
        tangoattrs.update({"scanID": environment["scanID"]})
        tangoattrs.update({"detectors": environment["detectors"]})
        if environment["start_time"]:
            tangoattrs.update({"start time": environment["start_time"]})
        if environment["end_time"]:
            tangoattrs.update({"end time": environment["end_time"]})

        for item in self.config:
            if self.begin:
                begin_value = self.begin[item["Group"]][item["Name"]]
                if type(begin_value) == float and math.isnan(begin_value):
                    begin_value = "NaN"

            try:
                value, format = self._value_retriever.retrieve_value(item["Device"], item["Attribute"])
            except:
                value = ""

            try:
                value = format % value
            except:
                value = str(value)

            try:
                for mapped in item["Mapping"]:
                    if str(value) == str(mapped["oldValue"]):
                        value = mapped["newValue"]
                        if self.begin:
                            begin_value = mapped["newValue"]
            except KeyError:
                pass

            values = dict()
            if self.begin:
                values.update({"begin": {"value": begin_value, "unit": item["Unit"]}})
                values.update({"end": {"value": value, "unit": item["Unit"]}})
            else:
                values.update({"value": value, "unit": item["Unit"]})

            if item["Group"] == "":
                tangoattrs.update({item["Name"]: values})
            else:
                if item["Group"] not in tangoattrs:
                    tangoattrs.update({item["Group"]: {}})
                tangoattrs[item["Group"]].update({item["Name"]: values})

        return tangoattrs
