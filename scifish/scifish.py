import json
import uuid
from datetime import datetime
import logging

from scifish.config import KAFKA_CERTS_PATH, TARANTA_URL
from scifish.kafkaproducer import KafkaProducer
from scifish.scientificmetadata import ScientificMetadata
from scifish.scicat_data import Data
from scifish.environment_data import Environment
from scifish.fish import fish
from scifish.value_retriever import TarantaValueRetriever, TangoValueRetriever


class SciFish:
    def __init__(self, **kwargs):
        ssl_cafile = kwargs.get("ssl_cafile", KAFKA_CERTS_PATH + "ca.crt")
        ssl_certfile = kwargs.get("ssl_certfile", KAFKA_CERTS_PATH + "user.crt")
        ssl_keyfile = kwargs.get("ssl_keyfile", KAFKA_CERTS_PATH + "user.key")

        self.scicat_data = None
        self.environment_data = None
        self.smd = None
        self.beamline = kwargs.get("beamline", None)
        self.test_enabled = kwargs.get("test", False)
        self.metaTimestampsEnabled = kwargs.get("metaTimestampsEnabled", True)
        self.pathfixer = kwargs.get("pathfixer", None)
        self.topic = None

        self._value_retriever = TangoValueRetriever()
        if self.beamline is not None:
            self._value_retriever = TarantaValueRetriever(TARANTA_URL + self.beamline + "/db")

        try:
            self.kafka = KafkaProducer(ssl_cafile, ssl_certfile, ssl_keyfile)
        except Exception as exc:
            raise Exception("Cannot connect to Kafka: %s" % str(exc))


    # ------------------------------------------------------------------------
    def start_scan(self, datasetName=None):
        creation_time = datetime.now()
        self.scicat_data = Data(uuid=str(uuid.uuid4()), creationTime=creation_time.timestamp(), beamline=self.beamline, pathfixer=self.pathfixer)
        self.beamline = self.scicat_data.event.replace("-experiment-began","")
        if self.test_enabled:
            self.topic = f"{self.beamline}-test"
            self.scicat_data.event = f"{self.topic}-experiment-began"
        else:
            self.topic = self.beamline

        if datasetName:
            self.scicat_data.datasetName = datasetName

        self.environment_data = Environment(start_time="",end_time="")
        if self.metaTimestampsEnabled:
            self.environment_data.start_time = creation_time.isoformat()

        self.smd = ScientificMetadata(self.beamline, self._value_retriever)


    # ------------------------------------------------------------------------
    def send_start(self):
        if self.scicat_data.datasetName == "":
            logging.warning("This dataset has no name! Please define SciFish().scicat_data.datasetName")
        else:
            try:
                self.scicat_data.scientificMetadata.update(self.smd.get(self.environment_data.to_dict()))
            except Exception as exc:
                logging.warning("No Scientific Metadata found: %s" % (exc))
            data = self.scicat_data.to_dict()
            self.kafka.producer.produce(self.topic, json.dumps(data).encode('utf-8'))
            self.kafka.producer.flush(1)
            self.scicat_data.event = self.scicat_data.event.replace("began", "ended")


    # ------------------------------------------------------------------------
    def end_scan(self):
        buffer = None
        if self.scicat_data.datasetName == "":
            logging.warning("This dataset has no name! Please define SciFish().scicat_data.datasetName")
        else:
            if self.metaTimestampsEnabled:
                self.environment_data.end_time = datetime.now().isoformat()
            try:
                self.scicat_data.scientificMetadata.update(self.smd.get(self.environment_data.to_dict()))
            except Exception as exc:
                logging.warning("No Scientific Metadata found: %s" % (exc))

            if "ended" in self.scicat_data.event:
                data = {x: self.scicat_data.to_dict()[x] for x in self.scicat_data.to_dict() if x in {"uuid", "event", "scientificMetadata"}}
            else:
                data = self.scicat_data.to_dict()

            logging.debug("Sending scan to kafka consumer...")
            self.kafka.producer.produce(self.topic, json.dumps(data).encode('utf-8'))
            buffer = self.kafka.producer.flush(1)
            if buffer > 0:
                logging.warning("There are %d items in the kafka Producer buffer!" % buffer)
                logging.info("SciCat metadata: %s" % data)
            else:
                logging.debug("SciCat metadata: %s" % data)

        return buffer


    # ------------------------------------------------------------------------
    def show(self):
        if self.scicat_data:
            return self.scicat_data.to_dict()
        else:
            logging.warning("The scan hasn't initialized yet. Please call SciFish.start_scan() before showing data")


    # ------------------------------------------------------------------------
    def fish(self):
        fish()
