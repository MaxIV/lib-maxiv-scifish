from functools import lru_cache
import json

import requests
import tango


class ValueRetrieverInterface:
    """
    A stub class that can act as an interface for a more concrete value
    retriever.
    """

    def retrieve_value(self, device_name, attribute_name) -> (str, str):
        raise NotImplementedError("subclass must provide a concrete implementation")


class TarantaValueRetriever(ValueRetrieverInterface):
    def __init__(self, taranta_url):
        self._taranta_url = taranta_url

    def retrieve_value(self, device_name, attribute_name) -> (str, str):
        query = (
            'query{device(name:"'
            + device_name
            + '"){attributes(pattern:"'
            + attribute_name
            + '"){value, format}}}'
        )

        query = json.dumps({"query": query})

        response = requests.post(self._taranta_url, data=query)
        response = json.loads(response.text)

        value = response["data"]["device"]["attributes"][0]["value"]
        format = response["data"]["device"]["attributes"][0]["format"]

        return value, format


@lru_cache(maxsize=None)
def get_device_proxy(device_name):
    return tango.DeviceProxy(device_name)


class TangoValueRetriever(ValueRetrieverInterface):
    def retrieve_value(self, device_name, attribute_name) -> (str, str):
        proxy = get_device_proxy(device_name)
        value = proxy.read_attribute(attribute_name).value
        format = proxy.get_attribute_config(attribute_name).format
        return value, format
