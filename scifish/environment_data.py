class Environment:
    __isfrozen = False
    def __init__(self, **kwargs):
        for key, value in template.items():
            setattr(self, key, value)

        self.__isfrozen = True

        for key, value in kwargs.items():
            setattr(self, key, value)

    def __setattr__(self, key, value):
        if self.__isfrozen and not hasattr(self, key):
            raise NotImplementedError(str(key) + " is not a valid environment key, valid keys are: 'detectors', 'scanID', 'title'.")
        object.__setattr__(self, key, value)

    def to_dict(self):
        return vars(self)

template = {
    "title": "",
    "scanID": "",
    "start_time": "",
    "end_time": "",
    "detectors": ""
}
