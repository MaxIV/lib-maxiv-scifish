import confluent_kafka
from scifish.config import BOOTSTRAP_SERVERS


class KafkaProducer(object):
    _instance = None
    def __new__(self, ssl_cafile, ssl_certfile, ssl_keyfile):
        if self._instance is None:
            self._instance = super(KafkaProducer, self).__new__(self)

            self.producer = confluent_kafka.Producer({
                'bootstrap.servers': BOOTSTRAP_SERVERS,
                'security.protocol': 'SSL',
                'ssl.ca.location': ssl_cafile,
                'ssl.key.location': ssl_keyfile,
                'ssl.certificate.location': ssl_certfile,
            })

        return self._instance
