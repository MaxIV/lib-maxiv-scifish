from tango import Database, DeviceProxy
import requests
import json

from scifish.config import PATH_FIXER_ATTRS, PATH_FIXER_PROPERTIES, TARANTA_URL

class UserInfo:

    def __init__(self, beamline = None, pathfixer = None):
        self.pathfixer_values = dict()
        self.beamline = beamline
        self.pathfixer = pathfixer


    def get(self):
        if self.beamline is None:
            self._get_info_from_pathfixer()
        else:
            self._get_info_from_graphql()

        info = {
            "sourceFolder": self.pathfixer_values["Path"],
            "description": "Recorded with the scicatrecorder at %s" % self.beamline,
            "proposalId": str(self.pathfixer_values["Proposal"]),
            "event": "%s-experiment-began" % str(self.beamline).lower()
        }

        return info


    def _get_info_from_graphql(self):
        url = TARANTA_URL + self.beamline + "/db"
        query = 'query{classes(pattern: "PathFixer"){devices{name attributes{name, value}}}}'
        query = json.dumps({"query":query})

        try:
            response = requests.post(url, data=query)
            response = json.loads(response.text)
        except json.JSONDecodeError:
            raise RuntimeError(f"Could not get the path info from Taranta, unknown beamline")

        device = None
        if self.pathfixer is None:
            device = response["data"]["classes"][0]["devices"][0]
        else:
            for d in response["data"]["classes"][0]["devices"]:
                if d["name"].lower() == self.pathfixer.lower():
                    device = d
                    break
        
        if device is None:
            s = "Could not find right pathfixer device."
            if self.pathfixer is not None:
                s += f"\nPlease check if the PathFixer device given is correct and running: {self.pathfixer}"
            raise RuntimeError(s)

        for attr in device["attributes"]:
            if attr["name"] == "path":
                self.pathfixer_values["Path"] = attr["value"]
            if attr["name"] == "proposal":
                self.pathfixer_values["Proposal"] = attr["value"]


    def _get_info_from_pathfixer(self):
        try:
            if self.pathfixer is None:
                db = Database()
                device_name = db.get_device_exported_for_class("PathFixer").value_string[0]
            else:
                device_name = self.pathfixer
            device_proxy = DeviceProxy(device_name)

            for attr in PATH_FIXER_ATTRS:
                self.pathfixer_values[attr] = device_proxy.read_attribute(attr).value

            self.pathfixer_values.update(device_proxy.get_property(PATH_FIXER_PROPERTIES))
            self.beamline = self.pathfixer_values["Beamline"][0]

        except Exception:
            s = "Cannot connect to PathFixer and determine the environment."
            if self.pathfixer is not None:
                s += f"\nPlease check if the PathFixer device given is correct and running: {self.pathfixer}"
            raise Exception(s)
