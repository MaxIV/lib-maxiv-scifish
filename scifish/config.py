BOOTSTRAP_SERVERS = 'bl-kafka-route-bootstrap-kafka-beamlines.apps.okd.maxiv.lu.se:8443'
KAFKA_CERTS_PATH = '/etc/ssl/certs/kafka/'
SCIDOG_URL = "https://scidog.maxiv.lu.se/api/config/active/"
TARANTA_URL = "https://taranta.maxiv.lu.se/"

PATH_FIXER_ATTRS = ["Path", "Proposal"]
PATH_FIXER_PROPERTIES = ["Beamline"]

WRITE_METADATA_BEGIN = ["danmax"]
