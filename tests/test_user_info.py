import pytest
from tango._tango import Database, DeviceProxy
import mocks

@pytest.fixture
def tango():
    input = 39
    return input


def test_get_info_from_pathfixer():
    from scifish.user_info import UserInfo

    Database.__init__ = mocks.Database.__init__  # monkeypatch tango stuff
    Database.get_device_exported_for_class = mocks.Database.get_device_exported_for_class
    DeviceProxy.__init__ = mocks.Device.__init__
    DeviceProxy.read_attribute = mocks.Device.read_attribute
    DeviceProxy.get_property = mocks.Device.get_property

    ui = UserInfo()
    ui._get_info_from_pathfixer()

    assert len(ui.pathfixer_values.items()) == 3
    assert ui.pathfixer_values["Beamline"] == ["CoSAXS"]

    value = mocks.Value("empty")
    value.value_string = []
    Database.get_device_exported_for_class = lambda x, y: value  # overwrite method to return empty list of devices

    with pytest.raises(Exception):  # check if empty list of devices raises exception
        ui._get_info_from_pathfixer()

def test_get_info_from_graphql():
    from scifish.user_info import UserInfo
    ui = UserInfo("cosaxs")
    ui._get_info_from_graphql()

    assert len(ui.pathfixer_values.items()) == 2
    assert "Path" in ui.pathfixer_values
    assert "Proposal" in ui.pathfixer_values
