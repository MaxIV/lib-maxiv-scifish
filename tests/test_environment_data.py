from unittest.mock import patch
import pytest

import mocks

@patch('pytz.timezone')
def test_init(mock_timezone):
    mock_timezone.return_value = None
    from scifish.environment_data import Environment

    e = Environment()
    e.detectors = ["det1", "det2"]

    env = e.to_dict()
    assert "title" in env
    assert "start_time" in env
    assert "end_time" in env
    assert env["detectors"] == ["det1", "det2"]


    with pytest.raises(NotImplementedError):
        e.UnknownKey = "unknown"
