from scifish.scifish import SciFish
from unittest.mock import patch, Mock

from scifish.value_retriever import TarantaValueRetriever, TangoValueRetriever

import mocks
from scifish.user_info import UserInfo

def test_scifish(caplog):
    UserInfo.get = mocks.get_user_info
    with patch("confluent_kafka.Producer") as kafka:
        scifish = SciFish()
    scifish.start_scan()
    scifish.fish()

    assert scifish.scicat_data.uuid != ""
    assert scifish.scicat_data.event == "cosaxs-experiment-began"
    assert scifish.scicat_data.sourceFolder == "/data/visitors/species/23456789/12345678/raw"
    assert scifish.scicat_data.proposalId == "20190679"

    scifish.end_scan()
    assert 'This dataset has no name!' in caplog.text
    assert scifish.scicat_data.scientificMetadata == {}

    scifish.scicat_data.datasetName = "Some Name"

    scifish.kafka.producer.flush = Mock(return_value=0)

    scifish.end_scan()

    assert "detectors" in scifish.scicat_data.scientificMetadata
    assert "start time" in scifish.scicat_data.scientificMetadata
    assert "end time" in scifish.scicat_data.scientificMetadata

def test_scifish_write_twice(caplog):
    with patch("confluent_kafka.Producer") as kafka:
        scifish = SciFish()
    scifish.start_scan()

    scifish.scicat_data.datasetName = "Some Name"

    scifish.send_start()

    assert scifish.scicat_data.event == "cosaxs-experiment-ended"

    scifish.kafka.producer.flush = Mock(return_value=0)

    scifish.end_scan()

    assert "detectors" in scifish.scicat_data.scientificMetadata
    assert "start time" in scifish.scicat_data.scientificMetadata
    assert "end time" in scifish.scicat_data.scientificMetadata
