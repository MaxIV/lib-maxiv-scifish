import datetime
import json
from requests.models import Response

from scifish.config import SCIDOG_URL

device_values = {
    "Path": "/data/visitors/species/23456789/12345678/raw",
    "Proposal": 23456789,
    "Beamline": ["CoSAXS"],

    "value": float("NaN"),
    "position": 0.3,
    "a_selected": True,
    "b_selected": False,
    "ea01_selected": False,
    "ea02_selected": True,
    "eb01_selected": False,
    "eb02_selected": False,
    "State": 2
}

scan_data = {
    "title": "ascan motor1 0 1 1 0",
    "start_time": datetime.datetime.now(),
    "end_time": datetime.datetime.now(),
    "scanID": 5,
    "detectors": "det1"
}

danmax_meta_config = {"data": [
    {
        "_id": "60d9bcb441ad59001ee09201",
        "created_at": "2021-06-28T12:12:36.684Z",
        "Group": "PXRD2D",
        "Name": "PXRD2D diode",
        "Device": "B304A-EA03/CTL/PNV-01",
        "Attribute": "State",
        "Unit": "",
        "Beamline": "danmax",
        "Mapping": [{"oldValue": 2, "newValue": "inserted"}],
        "__v": 0
    },
    {
        "_id": "60d9bcab41ad59001ee09139",
        "created_at": "2021-06-28T12:12:27.675Z",
        "Group": "Machine",
        "Name": "IVU gap",
        "Device": "b304a/ctl/ivu-r3-304-gap",
        "Attribute": "Position",
        "Unit": "",
        "Beamline": "danmax",
        "__v": 0
    },
]}

flexpes_meta_config = {"data": [
    {
        "_id": "60d975221653df001e7beeb6",
        "created_at": "2021-06-28T07:07:14.291Z",
        "Group": "",
        "Name": "Branch selected",
        "Device": "b107a-o03/opt/branch-selector-01",
        "Attribute": "a_selected",
        "Unit": "",
        "Beamline": "flexpes",
        "Mapping": [{"oldValue": "True", "newValue": "A"}],
        "__v": 0
    },
    {
        "_id": "60d9753d1653df001e7beeb9",
        "created_at": "2021-06-28T07:07:41.979Z",
        "Group": "",
        "Name": "A branch end station",
        "Device": "b107a-oa05/opt/endstation-selector-01",
        "Attribute": "ea01_selected",
        "Unit": "",
        "Beamline": "flexpes",
        "Mapping": [{"oldValue": "False", "newValue": "EA02"}],
        "__v": 0
    },
    {
        "_id": "60d975c51653df001e7beec8",
        "created_at": "2021-06-28T07:09:57.715Z",
        "Group": "Mono baffles position",
        "Name": "h gap",
        "Device": "B107A-O02/OPT/BAFF-01-HGAP",
        "Attribute": "position",
        "Unit": "",
        "Beamline": "flexpes",
        "__v": 0
    },
    {
        "_id": "60d976c01653df001e7beeef",
        "created_at": "2021-06-28T07:14:08.032Z",
        "Group": "Manipulator motor position EA01",
        "Name": "mp01 x",
        "Device": "motor/simstep_ctrl_0/1",
        "Attribute": "position",
        "Unit": "",
        "Beamline": "flexpes",
        "__v": 0
    },
]}

maxpeem_meta_config = {"data": [
    {
        "_id": "60d9bd8f41ad59001ee09210",
        "created_at": "2021-06-28T12:16:15.845Z",
        "Group": "",
        "Name": "Beamline Energy",
        "Device": "pm/beamline_energy_ctrl/1",
        "Attribute": "position",
        "Unit": "eV",
        "Beamline": "maxpeem",
        "__v": 0
    },
    {
        "_id": "60d9bd9141ad59001ee0923c",
        "created_at": "2021-06-28T12:16:17.670Z",
        "Group": "Microscope",
        "Name": "objective lens",
        "Device": "B111A-OA05/CTL/LEEM2000",
        "Attribute": "11",
        "Unit": "",
        "Beamline": "maxpeem",
        "__v": 0
    },
]}

veritas_meta_config = {"data": [
    {
        "_id": "60d9bff1bd57d9001e37d92c",
        "created_at": "2021-06-28T12:26:25.097Z",
        "Group": "M3A",
        "Name": "x",
        "Device": "pm/a_m3_5_leg_ctrl/1",
        "Attribute": "Position",
        "Unit": "",
        "Beamline": "veritas",
        "__v": 0
    },
    {
        "_id": "60d9bfefbd57d9001e37d918",
        "created_at": "2021-06-28T12:26:23.236Z",
        "Group": "",
        "Name": "mono gpit",
        "Device": "b316a-o02/opt/mono-01-gpit",
        "Attribute": "value",
        "Unit": "deg",
        "Beamline": "veritas",
        "__v": 0
    }
]}


class Device:
    def read_attribute(*args):
        return Value(args[0])

    def get_attribute_config(*args):
        return Format(args[0])

    def get_property(self, properties):
        properties_keys = list(set(device_values.keys()) & set(properties))
        properties = dict()

        for property in properties_keys:
            properties[property] = device_values[property]
        return properties


class Database:
    def __init__(self):
        pass

    def get_device_exported_for_class(self, name):
        return Value("some/tango/device")


class Value:
    def __init__(self, name):
        self.name = name
        self.value_string = [self.name]
        self.value = device_values[self.name] if self.name in device_values.keys() else "test_value"

class Format:
    def __init__(self, name):
        self.name = name
        self.format = "%s"


class KafkaProducer:
    def __init__(self, bootstrap_servers, value_serializer, security_protocol, ssl_cafile, ssl_certfile, ssl_keyfile):
        self.bootstrap_servers = bootstrap_servers
        self.value_serializer = value_serializer
        self.security_protocol = security_protocol
        self.ssl_cafile = ssl_cafile
        self.ssl_certfile = ssl_certfile
        self.ssl_keyfile = ssl_keyfile

        self.sent = list()

    def produce(self, topic, value):
        self.sent.append((topic, value))


def get_user_info(*args):
    return {
        "sourceFolder": "/data/visitors/species/23456789/12345678/raw", "description": "test test test",
        "proposalId": "20190679", "event": "cosaxs-experiment-began"}


def request_get(*args):
    request_url = args[0]

    if request_url == SCIDOG_URL + 'danmax':
        response_content = json.dumps(danmax_meta_config)
    elif request_url == SCIDOG_URL + 'flexpes':
        response_content = json.dumps(flexpes_meta_config)
    elif request_url == SCIDOG_URL + 'maxpeem':
        response_content = json.dumps(maxpeem_meta_config)
    elif request_url == SCIDOG_URL + 'veritas':
        response_content = json.dumps(veritas_meta_config)
    else: #default
        response_content = json.dumps(flexpes_meta_config)

    response = Response()
    response.status_code = 200
    response._content = str.encode(response_content)
    return response
