from unittest.mock import patch
import pytest

import mocks

@patch('pytz.timezone')
def test_init(mock_timezone):
    mock_timezone.return_value = None
    from scifish.user_info import UserInfo
    from scifish.scicat_data import Data

    UserInfo.get = mocks.get_user_info

    d = Data()
    data = d.to_dict()
    assert data["sourceFolder"] == "/data/visitors/species/23456789/12345678/raw"
    assert data["description"] == "test test test"
    assert data["proposalId"] == "20190679"
    assert data["event"] == "cosaxs-experiment-began"

    d = Data(datasetName="test", sampleId=123)
    data = d.to_dict()
    assert data["datasetName"] == "test"
    assert data["sampleId"] == 123

    with pytest.raises(NotImplementedError):
        d.UnknownKey = "unknown"

    d.comment = "But I can add some comment"
    data = d.to_dict()
    assert "comment" in data
