import pytest
from unittest import mock

import requests
import tango

from scifish.value_retriever import (
    ValueRetrieverInterface,
    TarantaValueRetriever,
    TangoValueRetriever,
)
import mocks


def test_value_retriever_abstract():
    with pytest.raises(NotImplementedError):
        ValueRetrieverInterface().retrieve_value("a", "b")


def test_value_retriever_taranta():
    def mock_post(url, data=None):
        class response:
            def __init__(self, text):
                self.text = text

        return response(
            """{"data": {"device": {"attributes": [{"value": 5, "format": "%f"}]}}}"""
        )

    # mock requests lib, uck...
    requests.post = mock_post

    vr = TarantaValueRetriever("base-url")
    value, format = vr.retrieve_value("a", "b")

    assert value == 5
    assert format == "%f"


@mock.patch("tango.DeviceProxy.__init__", side_effect=mocks.Device.__init__)
@mock.patch("tango.DeviceProxy.read_attribute", side_effect=mocks.Device.read_attribute)
@mock.patch(
    "tango.DeviceProxy.get_attribute_config",
    side_effect=mocks.Device.get_attribute_config,
)
def test_value_retriever_tango(*args):
    vr = TangoValueRetriever()
    value, format = vr.retrieve_value("a_selected", "b")

    assert value == "test_value"
    assert format == "%s"
