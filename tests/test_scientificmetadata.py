from tango import DeviceProxy
from unittest import mock
import mocks
import requests

from scifish.scientificmetadata import ScientificMetadata
from scifish.config import SCIDOG_URL

class MockValueRetriever:
    def __init__(self, config):
        self._config = config

    def retrieve_value(self, device_name, attribute_name):
        return self._config[attribute_name], None

@mock.patch('requests.get', side_effect=mocks.request_get)
@mock.patch('tango.DeviceProxy.__init__', side_effect=mocks.Device.__init__)
@mock.patch('tango.DeviceProxy.read_attribute', side_effect=mocks.Device.read_attribute)
@mock.patch('tango.DeviceProxy.get_attribute_config', side_effect=mocks.Device.get_attribute_config)
class TestScientificMetadata():
    def test_cosaxs(self, *args):
        data = mocks.scan_data

        smd = ScientificMetadata("cosaxs", MockValueRetriever(mocks.device_values))
        smd_cosaxs = smd.get(data)

        assert smd_cosaxs["title"] == "ascan motor1 0 1 1 0"
        assert smd_cosaxs["scanID"] == 5
        assert "start time" in smd_cosaxs
        assert "end time" in smd_cosaxs
        assert smd_cosaxs["detectors"] == "det1"

    def test_flexpes(self, *args):
        data = mocks.scan_data

        smd = ScientificMetadata("flexpes", MockValueRetriever(mocks.device_values))
        smd_flexpes = smd.get(data)

        assert smd_flexpes["title"] == "ascan motor1 0 1 1 0"
        assert smd_flexpes["scanID"] == 5
        assert "start time" in smd_flexpes
        assert "end time" in smd_flexpes
        assert smd_flexpes["detectors"] == "det1"

        assert "Mono baffles position" in smd_flexpes

        assert smd_flexpes["Branch selected"]["value"] == "A"
        assert smd_flexpes["A branch end station"]["value"] == "EA02"
        assert smd_flexpes["Manipulator motor position EA01"]["mp01 x"]["value"] == str(0.3)

    def test_maxpeem(self, *args):
        data = mocks.scan_data

        smd = ScientificMetadata("maxpeem", MockValueRetriever(mocks.device_values))
        smd_maxpeem = smd.get(data)

        assert "start time" in smd_maxpeem
        assert "Beamline Energy" in smd_maxpeem
        assert "Microscope" in smd_maxpeem

    def test_veritas(self, *args):
        data = mocks.scan_data

        smd = ScientificMetadata("veritas", MockValueRetriever(mocks.device_values))
        smd_veritas = smd.get(data)

        assert "start time" in smd_veritas
        assert "M3A" in smd_veritas
        assert "mono gpit" in smd_veritas
        assert smd_veritas["mono gpit"]["value"] == "nan"

    def test_danmax(self, *args):
        data = mocks.scan_data

        smd = ScientificMetadata("danmax", MockValueRetriever(mocks.device_values))
        smd_danmax = smd.get(data)

        assert "start time" in smd_danmax
        assert "end time" in smd_danmax
        assert "Machine" in smd_danmax
        assert smd_danmax["PXRD2D"]["PXRD2D diode"]["begin"]["value"] == "inserted"
        assert smd_danmax["PXRD2D"]["PXRD2D diode"]["end"]["value"] == "inserted"

    def test_cosaxs_use_taranta(self, *args):
        data = mocks.scan_data

        smd = ScientificMetadata("cosaxs", MockValueRetriever(mocks.device_values))
        smd_cosaxs = smd.get(data)

        assert smd_cosaxs["title"] == "ascan motor1 0 1 1 0"
        assert smd_cosaxs["scanID"] == 5
        assert "start time" in smd_cosaxs
        assert "end time" in smd_cosaxs
        assert smd_cosaxs["detectors"] == "det1"
