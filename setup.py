from setuptools import setup, find_packages

tests_require = ["pytz"],

setup(
    name="scifish",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    packages=find_packages(),
    extras_require={"tests": tests_require},
    install_requires=["confluent-kafka", "requests", "pytango"],
    author="KITS - Controls",
    author_email="KITS@maxiv.lu.se",
    license="GPL-3.0-or-later",
    url="https://gitlab.maxiv.lu.se/kits-maxiv/scicat/lib-maxiv-scifish",
)
